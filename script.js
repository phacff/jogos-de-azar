const   pedra = document.getElementById("pedraImg"),
        papel = document.getElementById("papelImg"),
        tesoura = document.getElementById("tesouraImg"),
        sorteio = document.getElementById("sorteioBtn"),
        pcResImg = document.getElementById("resImg"),
        resp = document.getElementById("resposta"),
        stat = document.getElementById("status");
let     escolha;

pedra.onclick=function() {
    escolha='Pedra';
    stat.innerText= " ";
    resp.innerText="Faça sua Escolha";
    pcResImg.src="./img/pedra.png";
}

papel.onclick=function() {
    escolha='Papel';
    stat.innerText= " ";
    resp.innerText="Faça sua Escolha";
    pcResImg.src="./img/pedra.png";
}

tesoura.onclick=function() {
    escolha='Tesoura';
    stat.innerText= " ";
    resp.innerText="Faça sua Escolha";
    pcResImg.src="./img/pedra.png";
}

sorteio.onclick=function(){
    if(escolha != undefined){ 
        function random() {
            return parseInt(Math.random() * (4 - 1) + 1);
        }

        let counter = random();
        //PEDRA==================================================================
        if(counter == 1) {
            counter='Pedra';
            pcResImg.src="./img/pedra.png";
            resp.innerText= counter + " x "+escolha;
            
            if(escolha=='Tesoura'){
                
                stat.innerText= "Você Perdeu, tente novamente!";
            }else if (escolha == 'Papel'){
                
                stat.innerText= "Você venceu! Meus parabens!";
            }else if(counter==escolha){
                stat.innerText= "EMPATE! Tente novamente";
            }
        //=======================================================================
        //PAPEL==================================================================   
        }else if(counter == 2){
            counter='Papel';
            pcResImg.src="./img/papel.png";
            resp.innerText= counter + " x "+escolha;

            if(escolha=='Pedra'){
                
                stat.innerText= "Você Perdeu, tente novamente!";
            }else if (escolha == 'Tesoura'){
                
                stat.innerText= "Você venceu! Meus parabens!";
            }else if(counter==escolha){
                stat.innerText= "EMPATE! Tente novamente";
            }
        //=======================================================================
        //TESOURA================================================================
        }else if(counter == 3){
            counter='Tesoura';
            pcResImg.src="./img/tesoura.png";
            resp.innerText= counter + " x " + escolha;

            if(escolha=='Papel'){
                
                stat.innerText= "Você Perdeu, tente novamente!";
            }else if (escolha == 'Pedra'){
                
                stat.innerText= "Você venceu! Meus parabens!";
            }else if(counter==escolha){
                stat.innerText= "EMPATE! Tente novamente";
            }

        }
        //=======================================================================
    }else{
        window.alert("ESCOLHA UMA OPÇÃO!");
        stat.innerText= " ";
        resp.innerText="Faça sua Escolha";
        pcResImg.src="./img/pedra.png";
        }
    //zera=======================================================================
    escolha=undefined;
    counter=undefined;
}